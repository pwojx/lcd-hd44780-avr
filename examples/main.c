#include <avr/io.h>

#include "lcd_44780.h"

const uint8_t anchor[8] = {
    0b11111,
    0b00100,
    0b00100,
    0b00100,
    0b10101,
    0b10101,
    0b10101,
    0b01110
};

const uint8_t mosaic[8] = {
    0b10101,
    0b01010,
    0b10101,
    0b01010,
    0b10101,
    0b01010,
    0b10101,
    0b01010
};

const uint8_t smiley_face[8] = {
    0b00000,
    0b01010,
    0b01010,
    0b01010,
    0b00000,
    0b10001,
    0b10001,
    0b01110
};

int main(void) {
    lcd_init();
    lcd_clear();

    lcd_set_pos(1, 0);
    lcd_print_str("Hello, world!");

    lcd_define_char(0, anchor);
    lcd_define_char(1, mosaic);
    lcd_define_char(2, smiley_face);

    lcd_set_pos(0, 1);
    lcd_print_hex(0xb5f);

    lcd_set_pos(6, 1);
    lcd_print_dec(1337);

    lcd_set_pos(13, 1);
    lcd_putc(0); // user-defined char 0
    lcd_putc(1); // user-defined char 1
    lcd_putc(2); // user-defined char 2

    while (1) {}
} 
